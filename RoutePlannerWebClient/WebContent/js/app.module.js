/*
Responsible:nitesh kumar
*/

var app=angular.module("RoutePlannerApp",['ui.router'])
.config(function($stateProvider,$urlRouterProvider){
	$stateProvider
	.state('RoutePlanner',{
		url:'/routeplanner',
		views:{
			"routeplan":{
				templateUrl:'templates/routeplanner.html',
				controller:'RoutePlannerCtrl'
			  }
		}
	}
	)	
	$urlRouterProvider.otherwise('/routeplanner');
});
